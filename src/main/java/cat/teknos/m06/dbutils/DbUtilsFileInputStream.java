package cat.teknos.m06.dbutils;

import java.io.IOException;
import java.io.InputStream;

public class DbUtilsFileInputStream extends InputStream {

    private final InputStream inputStream;
    public  DbUtilsFileInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @Override
    public int read( ) throws IOException {
        if (inputStream.available() < 10){
            return -1;
        }
    return inputStream.read();
    }

}
