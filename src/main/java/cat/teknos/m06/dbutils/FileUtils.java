package cat.teknos.m06.dbutils;

import cat.teknos.m06.dbutils.exception.InvalidSourceException;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileUtils {
     void toUtf8(ValidCharset charset, String from,String to) throws InvalidSourceException, IOException;

}
