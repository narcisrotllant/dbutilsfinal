package cat.teknos.m06.dbutils;

import cat.teknos.m06.dbutils.exception.InvalidSourceException;

import java.io.*;
import java.nio.charset.Charset;


public class FileUtilsImpl implements FileUtils{

    @Override
    public void toUtf8(ValidCharset validcharset, String from, String to) throws InvalidSourceException {
        var fileFrom = new File(from);
        var fileTo = new File(to);
        var charset = getCharset(validcharset);

        // TODO: tests
        try(var reader = new BufferedReader(new FileReader(fileFrom, charset)); var writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileTo), "UTF8"))){
           String line = null;
           while ((line = reader.readLine())!= null){
               writer.write(line);
           }
           writer.close();
            reader.readLine();
               //TODO: write with utf8 charset

        }catch(FileNotFoundException e) {
            System.out.println(e);
        } catch(IOException e) {

        }

    }
    protected boolean PathIsValid(String pathfrom, String pathto){
        if (pathfrom != ""){
            if(pathto != ""){
                File folderto = new File(pathto);
                File folderfrom = new File(pathfrom);
                if (folderto.isDirectory() && folderto.exists() && folderto.listFiles().length==0 && !folderfrom.getAbsolutePath().contains(" ") && folderfrom.isDirectory() && folderfrom.exists() && folderfrom.listFiles().length==0 && !folderfrom.getAbsolutePath().contains(" ") ){
                    return false;
                }
            }
        }
            return true;
    }

    private static Charset getCharset(ValidCharset validCharset){
        Charset charset;
        switch (validCharset){
            case ASCII:
                charset = Charset.forName("US-ASCII");
                break;
            case ISO8859:
                charset = Charset.forName("ISO-8859-1");
                break;
            case UTF8:
                charset = Charset.forName("UTF8");
                break;
            default:
                charset = Charset.forName("UTF16");
                break;
        }
        return charset;
    }

    public boolean isValidFile(){
        return false;
    }
}
