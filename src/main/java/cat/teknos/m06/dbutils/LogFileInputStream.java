package cat.teknos.m06.dbutils;

import java.io.IOException;
import java.io.InputStream;

public class LogFileInputStream extends InputStream {

        private final InputStream inputStream;
        public LogFileInputStream(InputStream inputStream){
            this.inputStream = inputStream;
        }

        @Override
        public int read() throws IOException {
            System.out.println("Reading...");
            return inputStream.read();
        }
}
