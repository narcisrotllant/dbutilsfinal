/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.teknos.m06.dbutils.exception;

/**
 *
 * @author uvic
 */
public class DbUtilException extends RuntimeException { // no checker, no s'ha de tractar

    public DbUtilException() {
    }

    public DbUtilException(String message) {
        super(message);
    }

    public DbUtilException(String message, Throwable cause) {
        super(message, cause);
    }

    public DbUtilException(Throwable cause) {
        super(cause);
    }

    public DbUtilException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
