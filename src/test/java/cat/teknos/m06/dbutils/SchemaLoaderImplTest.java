/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.teknos.m06.dbutils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author uvic
 */
public class SchemaLoaderImplTest {

    public SchemaLoaderImplTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of load method, of class SchemaLoaderImpl.
     */
    @Test
    public void testLoad() throws Exception {
        System.out.println("load");
        String path = "";
        SchemaLoaderImpl instance = new SchemaLoaderImpl();
        instance.load(path);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void PathIsValid() throws Exception {
        new FileUtilsImpl().PathIsValid("C:\\Users\\10010674\\OneDrive - Universitat de Vic\\UVIC 2.0\\Programes\\dbutilsnew\\src\\main\\resources\\utf8.txt", "C:\\Users\\10010674\\OneDrive - Universitat de Vic\\UVIC 2.0\\Programes\\dbutilsnew\\src\\main\\resources\\to.txt");
        Exception exception = assertThrows(NumberFormatException.class, () -> {
            new FileUtilsImpl().PathIsValid("C:\\Users\\10010674\\OneDrive - Universitat de Vic\\UVIC 2.0\\Programes\\dbutilsnew\\src\\main\\resources\\utf8.txt", "C:\\Users\\10010674\\OneDrive - Universitat de Vic\\UVIC 2.0\\Programes\\dbutilsnew\\src\\main\\resources\\to.txt");
        });
    }
}


